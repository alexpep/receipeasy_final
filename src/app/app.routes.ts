import { Routes } from '@angular/router';
import { LoginComponent } from './components/login/login.component';
import { NotFoundComponent } from './components/not-found/not-found.component';
import { RecipesComponent } from './components/recipes/recipes.component';
import { AuthGuardService } from './services/auth-guard.service';
import { RecipeDetailComponent } from './components/recipe-detail/recipe-detail.component';

export const routes: Routes = [
    {path: '', component: LoginComponent },
    {path: '', canActivate: [AuthGuardService],
         children: [{path: 'recipes', component: RecipesComponent },
                    {path: 'recipes/:id', component: RecipeDetailComponent}] 
                },
    {path: '**', component: NotFoundComponent }
    
];
