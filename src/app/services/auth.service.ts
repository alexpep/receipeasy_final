import { Injectable } from '@angular/core';
import { User } from '../models/user.model';
import { MarthaService } from './martha.service';
import { Observable, of } from 'rxjs';
import {map, catchError} from 'rxjs/operators'


@Injectable({
  providedIn: 'root'
})
export class AuthService {
  private _currentUser: User;

  get currentUser(): User{
    return this._currentUser;
  }
  constructor(private marthaService: MarthaService) {
    this._currentUser = JSON.parse(localStorage.getItem("CU"));
   }

  login(username: string, password: string): Observable<User>{
    
    // le subscribe permet d'avoir le retour async 

    return this.marthaService.select('login', { username , password }).pipe(
       map(users =>{ 
         if (users  && users.length ===1){
         const userData = users[0];

        this._currentUser = new User(userData.username, userData.id);
        localStorage.setItem("CU",JSON.stringify(this._currentUser));
         
         return this._currentUser;
       } else{
         return null;
       }
      }),
       catchError((error: any): Observable<any> => {
        console.error(error); 
        
        return of(null) })
     );
  }


  logout(){
    this._currentUser = null
    localStorage.setItem("CU",null);
  }
}

