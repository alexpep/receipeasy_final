import { Injectable } from '@angular/core';
import { HttpInterceptor, HttpRequest, HttpHandler, HttpEvent, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { tap } from 'rxjs/operators';


@Injectable({
  providedIn: 'root'
})
export class AuthIntercepterService implements HttpInterceptor {
  private readonly auth = btoa('pepin:88M0Obze')  ;
  constructor() { }


  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {

   const request = req.clone({headers: req.headers.set('auth',this.auth)});

   return next.handle(request).pipe(
     tap((response: HttpResponse<any>) => {
       console.log(request);
     })
   );
  }


}
