import { Injectable } from '@angular/core';
import { Recipe } from '../models/recipe.model';
import { Category } from '../enum/enum-category.enum';
import { User } from '../models/user.model';
import { AuthService } from './auth.service';
import { MarthaService } from './martha.service';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class RecipeService {

	private _recipes: Recipe[];
	private idIncrement: number;
	public categories = Category;
	
	
	get recipes(): Observable<Recipe[]> {
	  const currentUserId = this.authService.currentUser.id;
	  return this.martha.select('list',{'userId':currentUserId}).pipe(
		map(recipes => recipes || [])
	  );
	}
  
	constructor(private martha: MarthaService, private authService: AuthService) {
	  this._recipes = JSON.parse(localStorage.getItem('RECIPE')) || [];
	  this.idIncrement = +localStorage.getItem('AI')|| 0 ;
	 }
  
	 create(name: string, description: string, category: number): Observable<Recipe> {
	  const userId = this.authService.currentUser.id;
	  
	  return this.martha.insert('create', {category , name, description, userId}).pipe(
		map(id => {
		  if (id) {
			
			return new Recipe(id, name, description, category);
		  }
		  else {
			
			return null;
		  }
		})
	  );
	}
  
  
	delete(recipe: Recipe): Observable<boolean>{
		
	  return this.martha.delete('delete',{'id': recipe.id});
	}

	update(name: string, description: string, category: number, id: number){
		console.log('dans recipeService');
		return this.martha.select('update', {category , name, description, id}).pipe(
			map(success => {
			  if (success) {
				console.log('good job it update');
				return success;
			  }
			  else {
				
				return null;
			  }
			})
		  );

	}

	read(id: number): Observable<any>{

		return this.martha.read('read',{'id':id}).pipe(
			map(recipes => recipes )
		  );
	}
  
	get(id: number): Recipe{
	  return this._recipes.find(i => i.id === id);
	}
}
