import { Category } from '../enum/enum-category.enum';

		export class Recipe {

			id: number;
			name: string;
            description: string;
            category: number;
            
         

			constructor(id: number, name: string, description: string, category: number){
               
				this.name = name;
				this.description = description;
                this.id = id;
                this.category = category;
               

            }
        }