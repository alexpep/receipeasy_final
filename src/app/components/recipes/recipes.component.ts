import { Component, OnInit } from '@angular/core';
import { Recipe } from 'src/app/models/recipe.model';
import { RecipeService } from 'src/app/services/recipe.service';

@Component({
  selector: 'app-recipes',
  templateUrl: './recipes.component.html',
  styleUrls: ['./recipes.component.css']
})
export class RecipesComponent implements OnInit {
   recipes: Recipe[] =[];
 

  constructor(private recipesServices: RecipeService) {
    recipesServices.recipes.subscribe(recipes => this.recipes = recipes);
  }

  ngOnInit() {
  }

  recipeCreated(newRecipe: Recipe){
    this.recipes.push(newRecipe);
  }

  recipeDeleted(){
    this.recipesServices.recipes.subscribe(recipes => this.recipes = recipes);
  }

}
