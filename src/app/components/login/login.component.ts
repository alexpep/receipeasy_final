import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/services/auth.service';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
welcome: string;
titleLabel: string;
selectLabel: string;
loginLabel: string;
recipeLabel: string;

errorAlert: string;

  constructor(private authService: AuthService,private router: Router,public translate: TranslateService) {
    translate.addLangs(['en', 'fr']);
    translate.setDefaultLang('en');
    translate.use('en');
   }


  ngOnInit() {

    
  }


  login(username: string, password: string){
    // alert(`Hey! ${email}, ${password}`);
    
    this.authService.login(username, password).subscribe(user => { 
     
      if(user){
         this.router.navigate(['/recipes']);
       }
       else{
         this.translate.get('LOGIN.ERROR').subscribe(
           translation => {
             this.errorAlert = translation;
             alert(this.errorAlert);
           }
         )
         }
         //console.log(user)
       });
       
     }

}
