import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';
import { Recipe } from 'src/app/models/recipe.model';
import { RecipeService } from 'src/app/services/recipe.service';

import { Router } from '@angular/router';
import { Category } from 'src/app/enum/enum-category.enum';

@Component({
  selector: '[app-recipe-row]',
  templateUrl: './recipe-row.component.html',
  styleUrls: ['./recipe-row.component.css']
})
export class RecipeRowComponent implements OnInit {
  @Input() recipe: Recipe;
  @Output() recipeDelete: EventEmitter<any> = new EventEmitter();
  public category = Category;
  constructor(private recipeService: RecipeService,private router: Router) {
    
   }

  ngOnInit() {
  }

  view(){
    this.router.navigate(['/recipes/', this.recipe.id]);

  }

  delete(){
    this.recipeService.delete(this.recipe).subscribe(success=>this.recipeDelete.emit());;
  }

}
