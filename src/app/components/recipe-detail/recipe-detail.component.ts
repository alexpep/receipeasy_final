import { Component, OnInit, ɵConsole, TestabilityRegistry } from '@angular/core';
import { Recipe } from 'src/app/models/recipe.model';
import { ActivatedRoute, Router } from '@angular/router';
import { RecipeService } from 'src/app/services/recipe.service';
import { FormBuilder, FormControl, Validators, FormGroup } from '@angular/forms';
import { Category } from 'src/app/enum/enum-category.enum';
import { AuthService } from 'src/app/services/auth.service';


@Component({
  selector: 'app-recipe-detail',
  templateUrl: './recipe-detail.component.html',
  styleUrls: ['./recipe-detail.component.css']
})
export class RecipeDetailComponent implements OnInit {
  recipeForm: FormGroup;
  nameControl: FormControl;
  descriptionControl: FormControl;
  categoryControl: FormControl;

   categories =  Category;
  public categoryOptions = [];
  recipe: Recipe;

  id: number;


  constructor(formBuilder: FormBuilder, private recipesService: RecipeService,route: ActivatedRoute, private authService: AuthService, private router: Router) {
    this.nameControl = new FormControl('', Validators.required);
    this.descriptionControl = new FormControl('', Validators.required);
    this.categoryControl = new FormControl('',Validators.required);
    this.recipeForm = formBuilder.group(
      {
        name: this.nameControl,
        description: this.descriptionControl,
        category: this.categoryControl
      }
    );

    route.paramMap.subscribe(params =>{
      this.id =(Number (params.get('id')));
    });
}


update(){
  if(this.recipeForm.valid){
    this.recipesService.update(this.recipeForm.controls.name.value,
                               this.recipeForm.controls.description.value,
                               this.recipeForm.controls.category.value,
                               this.id)
    .subscribe(success =>{
      if(success)
      {
        console.log('asdasdasd')
        this.router.navigate(['/recipes']);
        return success;
      }
      else{
        return null;
      }
    } );

    /*this.recipe.category = this.recipeForm.controls.category.value;
    this.recipe.name = this.recipeForm.controls.name.value;
    this.recipe.description = this.recipeForm.controls.description.value;
    
    //this.recipesService.update();*/
    this.router.navigate(['/recipes']);
    
  }


}



  ngOnInit() {
    this.recipesService.read(this.id).subscribe(recipes => {
      this.recipeForm.controls.category.setValue(recipes[0].category);
      this.recipeForm.controls.name.setValue(recipes[0].name);
      this.recipeForm.controls.description.setValue(recipes[0].description);
    });
   
    this.categoryOptions =  Object.keys(this.categories);
  }

}
