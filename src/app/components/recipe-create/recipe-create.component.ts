import { Component, OnInit, ViewChild, EventEmitter, Output } from '@angular/core';
import { FormGroup, FormControl, FormBuilder, Validators, NgForm } from '@angular/forms';
import { RecipeService } from 'src/app/services/recipe.service';
import { Category } from 'src/app/enum/enum-category.enum';
import { Recipe } from 'src/app/models/recipe.model';


@Component({
  selector: 'app-recipe-create',
  templateUrl: './recipe-create.component.html',
  styleUrls: ['./recipe-create.component.css']
})
export class RecipeCreateComponent implements OnInit {
  @Output() readonly recipeCreate: EventEmitter<Recipe> =  new EventEmitter();

  @ViewChild('form', {static: true}) formElement: NgForm;
  recipeForm: FormGroup;
  nameControl: FormControl;
  descriptionControl: FormControl;
  categoryControl: FormControl;

   categories =  Category;
  public categoryOptions = [];
  
  constructor(formBuilder: FormBuilder, private recipesService: RecipeService) {
        this.nameControl = new FormControl('', Validators.required);
        this.descriptionControl = new FormControl('', Validators.required);
        this.categoryControl = new FormControl('',Validators.required);
				this.recipeForm = formBuilder.group(
					{
						name: this.nameControl,
            description: this.descriptionControl,
            category: this.categoryControl
					}
				);
   }

  ngOnInit() {
    this.categoryOptions =  Object.keys(this.categories);
  }

  create(){
    if(this.recipeForm.valid){
      const recipeValues = this.recipeForm.value;
      this.recipesService.create(recipeValues.name, recipeValues.description, recipeValues.category).subscribe(newRecipe =>{
        if(newRecipe){
          this.recipeCreate.emit(newRecipe);
        } else {
          alert('Could not complete create...')
        }
      });
      this.formElement.resetForm();
      this.recipeForm.controls.category.setValue('');
    }
  }

}
