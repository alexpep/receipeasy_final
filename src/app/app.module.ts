import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import {MarthaService } from './services/martha.service' ;
import { AuthIntercepterService } from './services/auth-intercepter.service';
import {TranslateModule, TranslateLoader} from '@ngx-translate/core';
import {TranslateHttpLoader} from '@ngx-translate/http-loader';

import { AppComponent } from './app.component';
import { AuthService } from './services/auth.service';
import { AuthGuardService } from './services/auth-guard.service';
import { routes } from './app.routes';
import { RouterModule } from '@angular/router';
import { LoginComponent } from './components/login/login.component';
import { NavComponent } from './components/nav/nav.component';
import { NotFoundComponent } from './components/not-found/not-found.component';
import { RecipesComponent } from './components/recipes/recipes.component';
import { RecipeCreateComponent } from './components/recipe-create/recipe-create.component';
import { RecipeService } from './services/recipe.service';
import { RecipeRowComponent } from './components/recipe-row/recipe-row.component';
import { RecipeDetailComponent } from './components/recipe-detail/recipe-detail.component';
import { HTTP_INTERCEPTORS, HttpClientModule, HttpClient } from '@angular/common/http';

export function HttpLoaderFactory(httpClient: HttpClient) {
  return new TranslateHttpLoader(httpClient);
}

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    NavComponent,
    NotFoundComponent,
    RecipesComponent,
    RecipeCreateComponent,
    RecipeRowComponent,
    RecipeDetailComponent
  ],
  imports: [
    BrowserModule,
    RouterModule.forRoot(routes),
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory, 
        deps: [HttpClient]
      }
    })
  ],
  providers: [AuthService,AuthGuardService,RecipeService,MarthaService,
    {provide: HTTP_INTERCEPTORS, useClass: AuthIntercepterService, multi: true}],
  bootstrap: [AppComponent]
})
export class AppModule { }
